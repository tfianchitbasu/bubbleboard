package com.example.umesh.omrscanner;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by umesh on 29-Aug-17.
 */

public class OngoingFragment extends Fragment {

    Context context;
    FirebaseDatabase database;
    DatabaseReference myRefSurveys,myRefUsers,myRefResponses,myRefOrg;
    RecyclerView ongoingSurveysRecyclerView;
    RecyclerAdapter ongoingSurveysRecyclerAdapter;
    LinearLayoutManager mOngoingSurveysLinearLayoutManager;
    ArrayList<SurveysResponseInfo> ongoingSurveysArrayList;
    int status;
    ArrayList<String> questions;
    ArrayList<String> questionType;
    SharedPreferences pref;
    String USERID,CITY,SCHOOL,ORGANIZATION;
    String surveyTitle,surveyID,classId,className;
    Long numberOfResponses=0l;
    String a;

    public OngoingFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView= inflater.inflate(R.layout.fragment_ongoing_survey, container, false);
        pref=getContext().getSharedPreferences(getString(R.string.shared_preferences), MODE_PRIVATE);
        USERID=pref.getString(getString(R.string.sp_user_id),"");
        ongoingSurveysArrayList=new ArrayList<>();
        database=FirebaseDatabase.getInstance();
        myRefSurveys=database.getReference("surveys");
        myRefUsers=database.getReference("users");
        myRefResponses=database.getReference("responses");
        ongoingSurveysRecyclerAdapter=new RecyclerAdapter(ongoingSurveysArrayList);
        ongoingSurveysRecyclerView=(RecyclerView)rootView.findViewById(R.id.ongoing_surveys_recycler_view);
        mOngoingSurveysLinearLayoutManager=new LinearLayoutManager(getContext());
        questions=new ArrayList<String>();
        questionType=new ArrayList<String>();
        ongoingSurveysRecyclerView.setLayoutManager(mOngoingSurveysLinearLayoutManager);
        ongoingSurveysRecyclerView.setAdapter(ongoingSurveysRecyclerAdapter);
        ongoingSurveysRecyclerView.setItemAnimator(new DefaultItemAnimator());
        ongoingSurveysRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        CITY=pref.getString(getString(R.string.sp_city),"");
        SCHOOL=pref.getString(getString(R.string.sp_school),"");
        ORGANIZATION=pref.getString(getString(R.string.sp_org),"");

        myRefResponses.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot dsp: dataSnapshot.getChildren()){

                    if(dsp.getKey().equals(USERID)){
                        for(DataSnapshot d:dsp.getChildren()){
                            a=d.getKey();
                            final DataSnapshot y=d;

                            myRefSurveys.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnap) {
                                    for(DataSnapshot ds:dataSnap.getChildren()){

                                        //dialog.show();
                                        if(ds.getKey().equals(y.getKey())){

                                            //if(dsp.child("status").getValue().toString().equals("1")) {
                                            surveyTitle = ds.child("name").getValue().toString();
                                            status=Integer.parseInt(ds.child("status").getValue().toString());
                                            populateRecyclerView(surveyTitle,status,y);
                                        }
                                    }

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        }
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        ongoingSurveysRecyclerAdapter.SetOnItemClickListener(new RecyclerAdapter.SurveyResponseRecyclerItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Intent i=new Intent(getActivity(),SurveyAnalysisActivity.class);
                i.putExtra("survey_name",ongoingSurveysArrayList.get(position).getTitle());
                i.putExtra("class_id",ongoingSurveysArrayList.get(position).getClassName());
                i.putExtra("status_survey",status);
                startActivity(i);
            }
        });


        return rootView;
    }

    public void populateRecyclerView(String surveyTitle,int status,DataSnapshot d){

        for(DataSnapshot ds:d.getChildren()){
            classId=ds.getKey();

            numberOfResponses=ds.getChildrenCount();
            Log.v("Number of responses", String.valueOf(numberOfResponses));
            SurveysResponseInfo surveysResponseInfo=new SurveysResponseInfo(surveyTitle,classId,status,numberOfResponses);
            if(status==1){
                ongoingSurveysArrayList.add(surveysResponseInfo);
                ongoingSurveysRecyclerAdapter.notifyDataSetChanged();
            }

        }
    }




}
